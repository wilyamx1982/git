# Git Commands

[Git config tutorial](https://www.youtube.com/watch?v=RVia18FNHYo)

### Config file

	git config --list
	git config --list --local
	git config --list --global
	
	git config --local --edit
	
### Initialize Repository

	git clone https://github.com/wilyamx/tawk-github-users.git
	git clone https://wilyamx1982@bitbucket.org/wilyamx1982/git.git
