# PROJECT SETUP #

## Connecting GitHub with Access Token

1. Enable [SAML](https://github.com/orgs/discovery-ltd/sso) to your account
1. Access the repository [here](https://github.com/discovery-ltd/v1-mobile-ios)
1. **Fork** above repository going to your Github account
1. SSO enabled personal [access token](https://github.com/settings/tokens)
	* [steps here](https://gist.github.com/aeosys/e013268de0e47944abd34d81be13556c)
1. Enable the SSO access token
1. Go to new assigned root folder via terminal  
1. Git clone into the current folder
		
		git clone https://<token>@github.com/owner/repo.git ./
		git clone https://<token>@github.com/P02WILLIAMR/v1-mobile-ios.git ./
		git clone https://<token>@github.com/wilyamx/thinkittwice-app ./

1. Drag and drop the repo folder above to SourceTree **Local** directories

## Connecting GitHub with SSH

[Connecting with SSH](https://docs.github.com/en/authentication/connecting-to-github-with-ssh)

## GitHub Markdown

	<p float="left">
		<img src="images/login.png" alt="Login" height="400">
		<img src="images/registration.png" alt="Registration" height="400">
		<img src="images/splashscreen.png" alt="Splashscreen" height="400">
	</p>

## SourceTree Setup
		
1. **Accounts**

	* **Auth Type:** OAuth
		* **Host:** `GitHub`
		* **Username:** `P02WILLIAMR`
		* **Password:** `<Github Access Token>`
		* **Protocol:** `HTTPS`

	* **Auth Type:** Personal Access Token
		* **Host:** `GitHub`
		* **Username:** `P02WILLIAMR`
		* **Personal Access Token:** `<Github Access Token>`
		* **Protocol:** `HTTPS`

	* **Auth Type:** OAuth
		* **Host:** `Bitbucket`
		* **Username:** `wilyamx1982`
		* **Protocol:** `HTTPS`

1. **Clone** `discovery-ltd / v1-mobile-ios` from **Remote** tab

## Forking

1. **Settings / Remotes / Edit Config File...**

		[remote "upstream"]
			url = https://github.com/discovery-ltd/v1-mobile-ios
			fetch = +refs/heads/*:refs/remotes/upstream/*
			
1. **Remote repository paths**

	* **origin:** `https://<access token>@github.com/P02WILLIAMR/v1-mobile-ios.git`
	* **upstream:** `https://github.com/discovery-ltd/v1-mobile-ios`
	
### Pull updates from upstream

1. Go to your [fork repository](https://github.com/discovery-ltd/v1-mobile-ios)
1. **Select an upstream branch from dropdown list**
	* `dev_mli`
	* `feature/dev_mli/hcom`
1. Perform `Fetch upstream` / `Fetch and merge`
1. Pull local branch via Source Tree

## Rebase

### Pull updates from develop branch using Rebase

### Merge and Amend of Commits

```
$ git reset --soft

$ git commit --amend --no-edit
$ git commit --amend --no-edit -m "ARST-"

$ git push origin HEAD -f
```

```
$ git rebase --continue
```

## Removing Git Locally

	rm -rf .git